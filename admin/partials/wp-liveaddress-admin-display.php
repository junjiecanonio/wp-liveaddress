<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Wp_Liveaddress
 * @subpackage Wp_Liveaddress/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
