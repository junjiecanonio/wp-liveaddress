<?php

/**
 * Fired during plugin activation
 *
 * @link       http://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Wp_Liveaddress
 * @subpackage Wp_Liveaddress/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Liveaddress
 * @subpackage Wp_Liveaddress/includes
 * @author     Alphasys <junjie@alphasys.com.au>
 */
class Wp_Liveaddress_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
