<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Wp_Liveaddress
 * @subpackage Wp_Liveaddress/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Liveaddress
 * @subpackage Wp_Liveaddress/public
 * @author     Alphasys <junjie@alphasys.com.au>
 */
class Wp_Liveaddress_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Liveaddress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Liveaddress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-liveaddress-public.css', array(), $this->version, 'all' );


	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Liveaddress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Liveaddress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-liveaddress-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ2FHNy5Ia_gv_AWsczJkuhrYDi9h7-EM&libraries=places&callback=initAutocomplete', array(), $this->version, false );

		//================================ Address Autocomplete Library =============================//
		
		// wp_enqueue_script('geocomplete' , plugin_dir_url( __FILE__ ) . 'js/jquery.geocomplete.min.js', array( 'jquery' ), $this->version, false );
		//================================ Address Autocomplete Library =============================//



		//================================ Address Validation =============================//
		// wp_enqueue_script('jquery.liveaddress.min' , plugin_dir_url( __FILE__ ) . 'js/jquery.liveaddress.min.js', array( 'jquery' ), $this->version, false );
		// wp_enqueue_script( 'liveaddress', plugin_dir_url( __FILE__ ) . 'js/liveaddress.js', array( 'jquery' ), $this->version, false );
		//================================ Address Validation =============================//

	}
	public function liveaddress(){
		global $title;
		require_once('partials/wp-liveaddress-public-display.php');
	}



}
