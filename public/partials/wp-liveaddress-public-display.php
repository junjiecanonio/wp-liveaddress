<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Wp_Liveaddress
 * @subpackage Wp_Liveaddress/public/partials
 */
print_r($WPCF7_ContactForm->id['408']);
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<form>
<!--     <input type="text" id="country" name="country"><br>
    <input type="text" id="street" name="street" ><br>
    <input type="text" id="city" name="city" ><br>
    <input type="text" id="state" name="state" ><br>
    <input type="text" id="ZIP" name="ZIP" ><br>
    <input type="submit" value="Submit Form"> -->

   <div id="locationField">
      <input id="autocomplete" placeholder="Enter your address"
             onFocus="geolocate()" type="text"></input>
    </div>

    <table id="address">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3"><input class="field" id="locality"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" disabled="true"></input></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>
</form>